import * as fastify from 'fastify'
import { CookieSerializeOptions } from 'cookie'

declare module 'fastify' {
  export interface FastifyRequest<HttpRequest> {
    session: Record<string, any>
  }
}

interface FastifyServerSessionOptions {
  secretKey?: string
  sessionCookieName?: string
  sessionMaxAge?: number
  cookie?: CookieSerializeOptions
}

declare const plugin: fastify.Plugin<any, any, any, FastifyServerSessionOptions>
export = plugin
